-- File Name: Grouper.purs
-- Description: Grouping logic
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 27 Mar 2023 12:34:00
-- Last Modified: 02 May 2023 23:12:21

module Grouper where

import Prelude

import Data.Maybe (Maybe(..))
import Data.Array (length, slice, filter, range, zip)
import Data.Foldable (maximum)
import Data.Int (toNumber, ceil)
import Data.String (Pattern(..), joinWith, split)
import Data.Tuple (fst, snd)

-- | splits a string of newline separated names to an array of names
parse :: String -> Array String
parse str =
  filter (\e -> e /= "") arr
  where
  arr = (Pattern "\n") `split` str

-- | splits an array into groups of size `n`
nSized :: forall a. Array a -> Int -> Array (Array a)
nSized names n =
  map (\i -> slice (i * n) ((i + 1) * n) names) (range 0 (count - 1))
  where
  count = ceil $ (toNumber $ length names) / toNumber n

-- | splits an array into `n` groups
sizeN :: forall a. Array a -> Int -> Array (Array a)
sizeN names n =
  filter (\e -> length e > 0) groups
  where
  count = length names `div` n
  remaining = length names `mod` n
  groups = map
    (\i -> slice (i * count + min i remaining) ((i + 1) * count + min (i + 1) remaining) names)
    (range 0 (n - 1))

-- | converts the array of names to a string list of groups
groupList :: Array (Array String) -> String
groupList groups =
  map (\e -> (show $ snd e) <> ": " <> fst e) z
    # joinWith "\n"
  where
  counter = range 1 (length groups)
  joined = map (\e -> joinWith ", " e) groups
  z = zip joined counter

-- | finds the maximum number of people in the group list
maxGroup :: Array (Array String) -> Int
maxGroup groups = case maximum n of
  Just m -> m
  Nothing -> 0
  where
  n = map (\e -> length e) groups

-- | exports groups to csv
toCsv :: Array (Array String) -> String
toCsv groups = header <> "\n" <> (map (\e -> (show $ snd e) <> "," <> fst e) z # joinWith "\n")
  where
  max = maxGroup groups
  header = "group,p" <> joinWith ",p" (map (\e -> (show e)) (range 1 max))
  counter = range 1 (length groups)
  joined = map (\e -> joinWith "," e) groups
  z = zip joined counter
