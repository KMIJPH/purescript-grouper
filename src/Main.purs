-- File Name: Main.purs
-- Description: Group builder
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 17 Mar 2023 21:17:47
-- Last Modified: 02 May 2023 23:12:36

module Main where

import Prelude

import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Exception (throw)
import UI (getElem, ids, run, download)
import Web.DOM.Element as E
import Web.Event.EventTarget (addEventListener, eventListener)
import Web.HTML (window)
import Web.HTML.Event.EventTypes (click)
import Web.HTML.HTMLDocument (toDocument)
import Web.HTML.Window (document)

main :: Effect Unit
main = do
  w <- window
  d <- document w
  let doc = toDocument d
  gen_button <- getElem doc ids.generate_button
  exp_button <- getElem doc ids.export_button

  gen_target <- case gen_button of
    Nothing -> throw $ "No element '" <> ids.generate_button <> "' found"
    Just b -> pure $ E.toEventTarget b

  exp_target <- case exp_button of
    Nothing -> throw $ "No element '" <> ids.export_button <> "' found"
    Just e -> pure $ E.toEventTarget e

  gen_listener <- eventListener run
  exp_listener <- eventListener download

  addEventListener click gen_listener true gen_target
  addEventListener click exp_listener true exp_target
