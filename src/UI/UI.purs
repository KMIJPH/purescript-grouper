-- File Name: UI.purs
-- Description: Effects
-- Author: KMIJPH
-- Repository: https://codeberg.org/KMIJPH
-- License: GPLv3+
-- Creation Date: 27 Mar 2023 21:07:51
-- Last Modified: 02 May 2023 23:13:05

module UI (run, download, getElem, ids) where

import Prelude

import Grouper (nSized, sizeN, parse, groupList, toCsv)

import Data.Array (sortWith)
import Data.Int (round)
import Data.Maybe (Maybe(..))
import Data.Traversable (traverse)
import Data.Tuple (fst, snd, Tuple(..))
import Effect (Effect)
import Effect.Console (log)
import Effect.Random (random)
import Web.DOM.Document as D
import Web.DOM.Element as E
import Web.DOM.Internal.Types (Element)
import Web.DOM.Node as N
import Web.DOM.NonElementParentNode (getElementById)
import Web.Event.Event (Event)
import Web.HTML (window)
import Web.HTML.HTMLDocument (toDocument)
import Web.HTML.HTMLElement as HE
import Web.HTML.HTMLInputElement as I
import Web.HTML.HTMLPreElement as P
import Web.HTML.HTMLSelectElement as S
import Web.HTML.HTMLTextAreaElement as T
import Web.HTML.Window (document)

foreign import _encodeURIComponent :: String -> String

-- | HTML Ids
type Ids =
  { mode_checkbox :: String
  , number_field :: String
  , input_field :: String
  , output_field :: String
  , generate_button :: String
  , export_button :: String
  }

ids :: Ids
ids =
  { mode_checkbox: "grouper-checkbox"
  , number_field: "grouper-number"
  , input_field: "grouper-input"
  , output_field: "grouper-output"
  , generate_button: "grouper-generate"
  , export_button: "grouper-export"
  }

-- | shuffles an array randomly
shuffle :: forall a. Array a -> Effect (Array a)
shuffle xs = map fst <<< sortWith snd <$> traverse (\x -> Tuple x <$> random) xs

-- | getElementById wrapper
getElem :: D.Document -> String -> Effect (Maybe Element)
getElem doc id = getElementById id $ D.toNonElementParentNode $ doc

-- | returns the numeric value of an input element
inputValueNum :: Maybe Element -> Effect (Maybe Number)
inputValueNum elem =
  case elem of
    Nothing -> pure Nothing
    Just element ->
      case I.fromElement element of
        Nothing -> pure Nothing
        Just input_element -> map Just (I.valueAsNumber input_element)

-- | returns the value of an textarea element
textAreaValue :: Maybe Element -> Effect (Maybe String)
textAreaValue elem = do
  case elem of
    Nothing -> pure Nothing
    Just element ->
      case T.fromElement element of
        Nothing -> pure Nothing
        Just textarea -> map Just (T.value textarea)

-- | returns the value of a select element
selectValue :: Maybe Element -> Effect (Maybe String)
selectValue elem = do
  case elem of
    Nothing -> pure Nothing
    Just element ->
      case S.fromElement element of
        Nothing -> pure Nothing
        Just select -> map Just (S.value select)

-- | sets the innerHTML of a pre element
setText :: Maybe Element -> String -> Effect Unit
setText elem val = do
  case elem of
    Nothing -> log "Not a valid element in: 'setText'"
    Just element ->
      case P.fromElement element of
        Nothing -> log "Not a valid element in: 'setText'"
        Just pre -> do
          let node = P.toNode pre
          N.setTextContent val node

-- | generates groups
generate :: Effect (Maybe (Array (Array String)))
generate = do
  w <- window
  d <- document w
  let doc = toDocument d
  textarea <- getElem doc ids.input_field
  checkbox <- getElem doc ids.mode_checkbox
  number <- getElem doc ids.number_field

  input_str <- textAreaValue textarea
  mode <- selectValue checkbox
  num_groups <- inputValueNum number

  case { input: input_str, mode: mode, num: num_groups } of
    { input: Just text, mode: Just m, num: Just n } -> do
      shuffled <- shuffle lines
      case m of
        "n_sized" -> pure $ Just (nSized shuffled (round n))
        "size_n" -> pure $ Just (sizeN shuffled (round n))
        _ -> pure Nothing
      where
      lines = parse text
    _ -> do
      log "Not enough or invalid arguments provided in: 'generate'"
      pure Nothing

-- | runs the grouping thing
run :: Event -> Effect Unit
run _ = do
  w <- window
  d <- document w
  let doc = toDocument d
  output <- getElem doc ids.output_field
  groups <- generate

  case groups of
    Just g -> setText output (groupList g)
    Nothing -> log "No groups found in: 'run'"

-- | downloads the groups as a txt
download :: Event -> Effect Unit
download _ = do
  w <- window
  d <- document w
  groups <- generate

  case groups of
    Just g -> do
      elem <- D.createElement "a" (toDocument d)
      E.setAttribute "href" ("data:text/plain;charset=utf-8," <> (_encodeURIComponent $ toCsv g)) elem
      E.setAttribute "download" "output.csv" elem
      case HE.fromElement elem of
        Just e -> HE.click e
        Nothing -> log "Invalid download button in: 'download'"
    Nothing -> do
      log "Nothing to download in: 'download'"
      pure unit
