module Test.Main where

import Prelude

import Effect (Effect)
import Grouper (nSized, parse, sizeN, groupList, maxGroup, toCsv)
import Test.Unit (test)
import Test.Unit.Assert (equal)
import Test.Unit.Main (runTest)

main :: Effect Unit
main = runTest do
  test "parse" do
    equal (parse s1) [ "James", "Ron", "Rodolfo S.", "Rudi" ]
    equal (parse s2) [ "Rudi", "Chiara", "Astrid" ]
  test "nSized" do
    equal (nSized g1 2) [ [ "Rudi", "Chiara" ], [ "Olf" ] ]
    equal (nSized g1 4) [ [ "Rudi", "Chiara", "Olf" ] ]
    equal (nSized g2 3) [ [ "Astrid", "Rudi", "Rodolfo" ], [ "Ron", "James", "Aster" ] ]
    equal (nSized g2 4) [ [ "Astrid", "Rudi", "Rodolfo", "Ron" ], [ "James", "Aster" ] ]
    equal (nSized g3 2) [ [ "Astrid", "Rudi" ], [ "Rodolfo", "Ron" ], [ "James", "Aster" ], [ "Olf" ] ]
    equal (nSized g3 3) [ [ "Astrid", "Rudi", "Rodolfo" ], [ "Ron", "James", "Aster" ], [ "Olf" ] ]
  test "sizeN" do
    equal (sizeN g1 2) [ [ "Rudi", "Chiara" ], [ "Olf" ] ]
    equal (sizeN g1 4) [ [ "Rudi" ], [ "Chiara" ], [ "Olf" ] ]
    equal (sizeN g2 3) [ [ "Astrid", "Rudi" ], [ "Rodolfo", "Ron" ], [ "James", "Aster" ] ]
    equal (sizeN g2 4) [ [ "Astrid", "Rudi" ], [ "Rodolfo", "Ron" ], [ "James" ], [ "Aster" ] ]
    equal (sizeN g3 2) [ [ "Astrid", "Rudi", "Rodolfo", "Ron" ], [ "James", "Aster", "Olf" ] ]
    equal (sizeN g3 5) [ [ "Astrid", "Rudi" ], [ "Rodolfo", "Ron" ], [ "James" ], [ "Aster" ], [ "Olf" ] ]
  test "groupList" do
    equal (groupList [ [ "Astrid", "Rudi" ], [ "Rodolfo", "Ron" ], [ "James", "Aster" ] ])
      "1: Astrid, Rudi\n2: Rodolfo, Ron\n3: James, Aster"
  test "maxGroup" do
    equal (maxGroup [ g1, g2, g3 ]) 7
    equal (maxGroup [ g1, g2 ]) 6
    equal (maxGroup [ g1 ]) 3
  test "toCsv" do
    equal (toCsv [ g1, g4 ]) "group,p1,p2,p3,p4\n1,Rudi,Chiara,Olf\n2,Ronnie,Andi,Rene,Michael"
  where
  s1 = "James\nRon\nRodolfo S.\nRudi"
  s2 = "\nRudi\nChiara\n\nAstrid"
  g1 = [ "Rudi", "Chiara", "Olf" ]
  g2 = [ "Astrid", "Rudi", "Rodolfo", "Ron", "James", "Aster" ]
  g3 = [ "Astrid", "Rudi", "Rodolfo", "Ron", "James", "Aster", "Olf" ]
  g4 = [ "Ronnie", "Andi", "Rene", "Michael" ]
